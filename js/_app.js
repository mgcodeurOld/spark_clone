(function(){

    document.addEventListener('DOMContentLoaded', function(){
        /*
        * add _show class to an element with data-toggle
        * the element cible is the element with class same as value of data-toggle
        * */
        const elementToggler = document.querySelectorAll('*[data-toggle]');
        for(let i = 0; i<elementToggler.length; i++){
            elementToggler[i].addEventListener('click', function(e){
                e.preventDefault();
                document.querySelector('.' + elementToggler[i].getAttribute('data-toggle'))
                    .classList.toggle('_show');
            })
        }

        /*
        * banner splide (slider)
        * */
        new Splide( '.splide' ).mount();
        var elms = document.getElementsByClassName( 'splide' );
        for ( var i = 0, len = elms.length; i < len; i++ ) {
            new Splide( elms[ i ] ).mount();
        }
    })

})()